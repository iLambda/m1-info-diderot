# Représentation des fonctions booléennes

Comme dit précédemment, on va s'intéresser aux fonctions booléennes $f : \{0, 1\}^n \to \{0, 1\}$ en les voyant comme des langages formels, par la bijection suivante :
$$
L(f) = \{ (x_1, ..., x_n) \mid f(x_1, ..., x_n) = 1 \}
$$

## Ordered binary decision diagrams

On va considérer des automates représentant des langages. Il y a beaucoup d'états inutiles : on va alors optimiser l'automate en plaçant directement les variables du problème aux noeuds, et en fusionnant les états terminaux.

On appellera **OBDD** l'automate résultant. Il sera défini comme :
* $V$ un ensemble fini de sommets
* $n \in \mathbb N$ le nombre de variables
* $i : V \to \{ 1, ..., n+1 \}$ le niveau des sommets
* $t : V \to V$ les fils représentant une valuation vraie ($\top$) d'un sommet
* $f : V \to V$ les fils représentant une valuation fausse ($\bot$) d'un sommet
* $val : \{ v \mid i(v) = n + 1 \} \to \{ 0, 1 \}$ les noeuds terminaux de niveau $n+1$


![](img/06-02-19_1.png)

Les flèches étiquetées par 0 seront dessinées en pointillés. Les flèches étiquetées par 1 seront pleines.

On a la propriété suivante : si un noeud $A$ est enfant d'un noeud $B$, on en déduit que $i(A) < i(B)$. Ainsi, il vient :
$$
\left \{
  \begin{align}
    i(t(v)) >& i(v)\\
    i(f(v)) >& i(v)
  \end{align}
\right.
$$

Un OBDD sera réduit (on l'appelle alors **ROBDD**) si, en notant $\varphi$ la fonction qu'il représente :
* (1) $\forall v_1, v_2, v_1$ et $v_2$ ne sont pas équivalents ($\varphi v_1 \neq \varphi v_2$)

* (2) $\forall v$, on a $f(v) \neq t(v)$

On remarque que $(1) \implies (2)$.

### Propriétés des ROBDD

On a les propriétés suivantes :

* Chaque fonction $\varphi$ peut être représentée par un **ROBDD**.

* Ce ROBDD est unique à isomorphisme près.

* Il existe un algorithme de réduction permettant d'obtenir un ROBDD a partir d'un OBDD donné (il lui sera évidemment équivalent).

* Il existe un algorithme pour construire un ROBDD pour la conjonction, la disjonction, la négation, et l'équivalence.

#### Algorithme de réduction

La procédure est la suivante :
* on monte du niveau $n+1$ au niveau $1$
* sur le niveau $n+1$ on fusionne les noeuds terminaux 0 et les noeuds terminaux 1 entre eux
* sur les autres
  * si $f(u) = t(u)$, on les fusionne
  * si $f(u) = f(v)$ et $t(u) = t(v)$, on fusionne $u$ et $v$

#### Construction de $\vee, \wedge, \neg, \iff$

* Pour la négation, il suffit d'inverser les noeuds terminaux 1 avec les noeuds terminaux 0.

* Pour l'intersection $\wedge$, on va réutiliser l'idée du "produit" d'automates.

  * Soit deux OBDD $\varphi, \psi$. On prend le premier noeud, respectivement étiqueté par $i$ et $j$. Ses fils vrais et faux sont respectivement $v, w$ et $\beta, \gamma$. On a les cas suivants :

      * Si $i < j$ on construit un OBDD dont le premier noeud est étiqueté par $i$, et dont les fils vrais et faux sont respectivement $v \wedge \psi$ et $w \wedge \psi$.

      * Si $i > j$, on fait le symétrique du cas $i < j$.

      * Si $i = j$ on construit un OBDD dont le premier noeud est étiqueté par $i$ (ou $j$), et dont les fils vrais et faux sont respectivement $v \wedge \beta$ et $w \wedge \gamma$.

### Avantages et inconvénients

* Points forts

  * C'est une structure de donnée relativement petite

  * Les opérations sont faciles

  * Les fonctions usuelles donnent de "jolis" ROBDD

* Points faibles

  * Certaines fonctions font exploser la taille du ROBDD ou le temps de réduction (sinon on pourrait résoudre $\operatorname{SAT}$ en temps polynomial).

  * Pour une même fonction $\varphi$, la taille du ROBDD dépend énormément de l'ordre des variables.

## Parenthèse sur les automates et langages réguliers

Soit $\Sigma, \Gamma$ deux alphabets.

On appelle $h : \Sigma \to \Gamma$ un **homomorphisme lettre à lettre**. On l'étends au mots $w$ de $\Sigma$ :
$$
\forall w = w_1...w_n \in \Sigma^\ast \qquad h(w) = h(w_1)h(w_2)...h(w_n)
$$

On définit :
$$
h(L) = \{ h(w) \mid w \in L \} \subseteq \Gamma^\ast
$$
$$
h^{-1}(L) = \{ w \in \Sigma^\ast \mid h(w) \in L \} \subseteq \Sigma^\ast
$$

On a le théorème suivant :

$$
L \in \operatorname{REG} \implies
\left\{
  \begin{align}
    h(L) &\in \operatorname{REG}\\
    h^{-1}(L) &\in \operatorname{REG}
  \end{align}
\right.
$$

Il suffit de prendre un automate pour $L$, de renommer les lettres par $h$. L'automate construit reconnait alors $h(L)$. De même pour $h^{-1}(L)$.

# Arithmétique de Presburger

L'**arithmétique de Presburger** est la théorie du premier ordre de $(\mathbb N, +, 0, 1, =)$. On ne considère pas la multiplication.

On a le théorème suivant :
> L'arithmétique de Presburger est **décidable**
