(* Imp identifier *)
type id = string
(* Imp expression *)
type _ expression =
  (* Integer expressions *)
  | Constant : int -> int expression
  | Sum : int expression * int expression -> int expression
  | Identifier : id -> int expression
  (* Boolean expressions *)
  | Less : int expression * int expression -> bool expression
(* Imp command *)
type command =
  | Skip
  | Assign of id * int expression
  | Sequence of command * command
  | Condition of bool expression * command * command
  | While of bool expression * command
(* Imp memory *)
type memory = (id * int) list
(* Imp continuation *)
type continuation =
  | Halt
  | Continue of command * continuation

(* Evaluate an imp expression *)
let rec evaluate : type a. memory -> a expression -> a =
  (* Match over expression *)
  fun mem expr -> match expr with
    (* Evaluate a constant *)
    | Constant n -> n
    (* Evaluate a sum *)
    | Sum (a, b) -> (evaluate mem a) + (evaluate mem b)
    (* Evaluate an identifier *)
    | Identifier id -> List.assoc id mem
    (* Evaluate a predicate *)
    | Less (a, b) -> (evaluate mem a) < (evaluate mem b)

(* The big step interpreter *)
let rec run command memory = match command with
  (* Skip *)
  | Skip -> memory
  (* Assign *)
  | Assign (id, expr) ->
    (* Evaluate expression *)
    let v = evaluate memory expr in
    (* Bind *)
    let mem' = (id, v)::(List.remove_assoc id memory) in
    (* Return modified memory *)
    mem'
  (* Sequence *)
  | Sequence (s1, s2) ->
    (* Run first *)
    let mem' = run s1 memory in
    (* Run second *)
    let mem'' = run s2 mem' in
    (* Return second *)
    mem''
  (* Condition *)
  | Condition (predicate, sthen, selse) ->
    (* Evaluate condition *)
    let branch_selector = evaluate memory predicate in
    (* Choose the branch *)
    let branch = if branch_selector
                 then sthen else selse in
    (* Run the branch *)
    run branch memory
  (* While *)
  | While (predicate, body) ->
    (* Evaluate predicate *)
    let pred_value = evaluate memory predicate in
    (* Check the predicate value *)
    if not pred_value
    (* Out of the loop *)
    then memory
    (* Run the body *)
    else run (Sequence (body, command)) memory

(* The small step interpreter *)
let step (command, continuation, memory) = match command with
  (* Skip *)
  | Skip -> begin match continuation with
      (* Terminal condition *)
      | Halt -> command, Halt, memory
      (* Nonempty continuation *)
      | Continue (s, k) -> s, k, memory
    end
  (* Sequence *)
  | Sequence (s1, s2) ->
    (* Add second command to continuation *)
    s1, (Continue (s2, continuation)), memory
  (* Assign *)
  | Assign (id, expr) ->
    (* Evaluate expression *)
    let v = evaluate memory expr in
    (* Bind *)
    let mem' = (id, v)::(List.remove_assoc id memory) in
    (* Return state w/ modified memory *)
    Skip, continuation, mem'
  (* Condition *)
  | Condition (predicate, sthen, selse) ->
    (* Evaluate condition *)
    let branch_selector = evaluate memory predicate in
    (* Choose the branch *)
    let branch = if branch_selector
                 then sthen else selse in
    (* Return *)
    branch, continuation, memory
  (* While *)
  | While (predicate, body) ->
    (* Evaluate predicate *)
    let pred_value = evaluate memory predicate in
    (* Check the predicate value *)
    if not pred_value
    (* Out of the loop *)
    then Skip, continuation, memory
    (* Run the body *)
    else body, (Continue (command, continuation)), memory

(* The closure of the small step rewriting relation *)
let rewrite command memory =
  let rec do_step = function
    (* Normal state ; no rewrite, return *)
    | (Skip, Halt, mem) as state -> state
    (* Non-normal state ; rewrite *)
    | state -> do_step (step state)
  in
  (* Rewrite into normal form *)
  let (_, _, mem) = do_step (command, Halt, []) in
  (* Return memory *)
  mem

(* Sample program *)
let (|:) x y = Sequence (x, y)
let (=:) x y = Assign (x, y)
let (<:) x y = Less (x, y)
let (+:) x y = Sum (x, y)
let countToTen =
  (* x := 0 *)
  ("x" =: Constant 0) |:
  (* while *)
  (While
    (* x < 10 *)
    (Identifier "x" <: Constant 10,
    (* do x := x + 1 *)
    "x" =: (Identifier "x" +: Constant 1)))
(* Large step interpreter test *)
let big_step_mem = run countToTen []
let () = print_int (List.assoc "x" big_step_mem); print_newline ()
(* Small step interpreter test *)
let small_step_mem = rewrite countToTen []
let () = print_int (List.assoc "x" small_step_mem); print_newline ()
