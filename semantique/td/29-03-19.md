# CPS en appel par valeur

On définit :
* $\psi(x) = x$

* $\psi(\lambda x. M) = \lambda x. \lambda k. (M \vert k)$

L'application $M \vert K$ peut être vue comme "l'évaluation de $M$ dans la continuation $K$". Elle est définie comme :

* $V \vert k = (k)V$

* $V \vert (\lambda x. M) = [\psi(V)/x]M$

* $M_0M_1 \vert K = M_0 \mid \lambda x_0. (M_1 \mid \lambda x_1. (x_0)(x_1)K)$

On a $M \vert K \in \operatorname{CPS}$ défini comme :
$$
\operatorname{CPS}(M)  = M \vert \lambda x. \operatorname{halt} x
$$

## Cps de $(\lambda x. \lambda y. x)z$

$$
\begin{align}
\operatorname{CPS}((\lambda x. \lambda y. x)z)
&= (\lambda x. \lambda y. x) z \mid \lambda x. \operatorname{halt} x \\
&= (\lambda xy. x) \mid \lambda x_0 (z \mid \lambda x_1. (x_0)(x_1)(\lambda x. \operatorname{halt} x)) \\
&= [\psi(\lambda xy. x)/x_0](z \mid \lambda x_1. (x_0)(x_1)(\lambda x. \operatorname{halt} x))\\
&= [\lambda x. \lambda k. (\lambda y. x \mid k) /x_0](z \mid \lambda x_1. (x_0)(x_1)(\lambda x. \operatorname{halt} x))\\
&= (z \mid \lambda x_1. (\lambda x. \lambda k. (\lambda y. x \mid k))(x_1)(\lambda x. \operatorname{halt} x))\\
&= (\lambda x. \lambda k. (\lambda y. x \mid k)) z (\lambda x. \operatorname{halt} x)
\end{align}
$$
