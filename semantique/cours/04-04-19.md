# Opérateurs de contrôle en CPS

On va s'intéresser a des opérateurs modifiant le flôt de contrôle.

## Extension de $\lambda_v$

On va définir les opérateurs $C$ pour **control** et $A$ pour **abort**.
La syntaxe est la suivante :
```
M ::= ... | CM | AM
E ::= [] | EM | VE
V ::= λid.M
```

On a les règles suivantes :
$$
\begin{align}
E[(\lambda x. M)V] &\to E[[V/x]M]\\
E[CM] &\to M[\lambda x. AE[x]]\\
E[AM] &\to M
\end{align}
$$

On peut utiliser ces opérateurs pour implémenter $\operatorname{catch}$ & $\operatorname{throw}$.

### Threads coopératifs

On va rajouter a $M$ :
```
M ::= ... | CM | AM | spawn M | stop | yield
```

* thread en exécution : $t \vdash t_1 ... t_n$
* spawn : $t' \vdash ... t_{n+1}$
* stop : $t_i \vdash ... \backslash t_i$
* yield : $t_i \vdash ... \backslash t_i \cup t$

L'objectif est de simuler le langage avec threads. On a un scheduler $\operatorname{Sch}$ doté des opérations suivantes :
* $emp : \operatorname{Sch}$
* $ins : \operatorname{Thread} \times \operatorname{Sch} \to \operatorname{Sch}$
* $rmv : \operatorname{Sch} \to \operatorname{Thread} \times \operatorname{Sch}$

Le type des termes devra $[\![M]\!] : \operatorname{Sch} \to \operatorname{Sch} \times \operatorname{Val}$

Les règles sont :
* $E[\operatorname{spawn} M], S \to E[()], S \cup \lambda. M$
* $E[\operatorname{yield}], S \to V(), Sch \setminus V \cup (\lambda. E[()])$

On va avoir :
* $[\![x]\!] = \lambda s. (s, x)$

* $[\![\lambda x. M]\!] = \lambda s. (s, \lambda x. [\![M]\!])$

* $[\![MN]\!] = \lambda s. \operatorname{let} \ (s_1, x_1) = [\![M]\!]s
 \operatorname{in let} (s_2, x_2) = [\![N]\!]s_1 \operatorname{in} x_1x_2s_2$[\![\operatorname{spawn} M]\!]

* $[\![\operatorname{spawn} M]\!] = \lambda s.(ins(\lambda.M, s), ())$

* $[\![\operatorname{yield}]\!] = \lambda s. C(\lambda K. \operatorname{let} (t, s_1) = rmv(\operatorname{ins}(K, s)) \operatorname{in} (s_1, t()))$

#### Mise en oeuvre

On rappelle :
$$
(\underbrace{M[\eta]}_{\text{clôture}}, \underbrace{s}_{\text{pile}})
$$

La sémantique à petit pas donne :
* $(x[\eta], s) \to (\eta(x), s)$

* $(MN[\eta], s) \to (M[\eta], arg : N[\eta] : s)$

* $(vc, arg : fun : s) \to (c, l : vc : s)$

* $(vc, l : (\lambda x. M)[\eta]: s) \to (M[\eta[vc/x]], s)$

On rajoute l'operation $\operatorname{ret}$ qui prends le contenu de la pile et la transforme en une valeur clôture.

* $(CM[\eta], s) \to (M[\eta], ret(s))$

* $(AM[\eta], s) \to (M[\eta], -)$
