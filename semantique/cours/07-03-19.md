# Inférence de type

L'**inférence de type** correspond au problème suivant : étant donné un terme $M$, on cherche un type $A$ tel que $\vdash M : A$.

## Inférence des types propositionnels

On peut montrer que l'inférence de types propositionnels se réduit a un problème d'unification syntaxique, et vice versa.

On rappelle les règles de typage dans un système à la Curry :
$$
\frac{x : A \in \Gamma}{\Gamma \vdash x:A}
$$

$$
\frac{\Gamma \vdash M : A \to B \qquad \Gamma \vdash N : A}{\Gamma \vdash MN : B}
$$

$$
\frac{\Gamma, x : A \vdash M : B}{\Gamma \vdash \lambda x. M : A \to B}
$$

En entrée, on va prendre un terme $M$ avec des variables libres. On associe à M un système de contraintes (équations entre termes du premier ordre) :
$M \rightsquigarrow Eq$.

On a en sortie deux possibilités :
* $\not \exists S \quad S \vDash Eq$ : ainsi, $M$ n'est pas typable
* $\exists S \text{ most general unifier t.q. } S \vDash Eq$. Ainsi, $M$ est typable.

### Génération d'équations

Supposons qu'on aie un lambda terme (pour l'exemple, on emploiera $M= \lambda xyz. (xy)(xz)$).

On a comme terme du premier ordre les mots générés par `S ::= id | S → S`. On va aussi s'assurer du fait que les variables liées sont différentes et différentes des variables libres.

On associe une variable de type à chaque terme :

![](img/07-03-19_1.png)

On a les égalités suivantes :
* pour les noeuds $\lambda$
  * $t_0 = t_x \to t_1$
  * $t_1 = t_y \to t_2$
  * $t_2 = t_z \to t_3$

* pour les noeuds applications $@$
  * $t_4 = t_5 \to t_3$
  * $t_x = t_y \to t_4$
  * $t_y = t_z \to t_5$

Après résolution, on trouve que :
$$
M : \overbrace{t_z \to (t_5 \to t_3)}^{x} \to \overbrace{(t_z \to t_5)}^y \to \overbrace{t_z}^z \to t_3
$$

# Equivalence entre $\lambda$-termes

On rappelle que lors de l'étude de $\operatorname{Imp}$, on avait défini une sémantique $[\![ S ]\!]^{IO} = \{(s, s') \mid (S, s) \Downarrow s' \}$. On avait la propriété importante de **préservation par contexte**, à savoir, pour tout contexte $C$, $S_1 =^{IO} S_2 \implies C[S_1] =^{IO} C[S_2]$

On va essayer de faire la même chose, mais pour le $\lambda$-calcul en appel par nom.

## Rappels sur l'appel par nom

On ne traite que des termes clos. On a les règles suivantes :
$$
\frac{}{V \Downarrow V}
$$
$$
\frac{M \Downarrow \lambda x. M' \quad [N/x]M' \Downarrow V}{MN \Downarrow V}
$$

On rappelle que $M \Downarrow$ signifie $M$ termine vis-à-vis de l'appel par nom.

## Pré-ordre contextuel

On observe la terminaison d'un terme clos. Posons :

$$
M \leqslant_C N \iff \forall C \text{ contexte fermant } ~ ~ C[M] \Downarrow \implies C[N] \Downarrow
$$

Montrons que cette relation est un préordre (réfléxive et transitive), stable par contexte, non triviale, et que $n \neq m \implies \underline{n} \leqslant_C \underline{m}$.

###Transitivité

Supposons $M_1 \leqslant_C M_2 \leqslant_C M_3$.
Prenons un contexte $C$ fermant pour $M_1$ $M_3$.
Si il est fermant pour $M_2$, c'est trivial.

Supposons que $C$ n'est pas fermant pour $M_2$, c'est à dire :
* $x \in \operatorname{FV}(M_2)$
* $x \notin \operatorname{FV}(M_1) \cup \operatorname{FV}(M_3)$

$C' = (\lambda x. C)I$ est fermant pour $M_1, M_2, M_3$. On a $C'[M_i] \Downarrow \iff C[M_i] \Downarrow$. On remarque alors que :
$C[M_1] \Downarrow \iff C'[M_1] \Downarrow \iff C'[M_2] \Downarrow \iff C'[M_3] \Downarrow \iff C[M_3]$.

Ainsi, on a $M_1 \leqslant_C M_3$.

### Stabilité par contexte

$C_1, C_2$ contextes $\implies C_1[C_2]$ contexte.
On a :
$$
\begin{align}
M \leqslant_C N &\implies C[M] \leqslant_C C[N]
\end{align}
$$

Or, $\forall C'$ fermant pour $C[M]$ et $C[N]$, $C'[C[M]]$ termine iff $C'[C[N]]$ termine.

### Non trivial

On va montrer que $\lambda xy. x \leqslant_C \lambda xy. y$.
Prenons le contexte suivant : $C = []I\Omega$, on a :
* $[\lambda xy. x]I\Omega \Downarrow$
* $[\lambda xy. y]I\Omega \Downarrow$

## Relation d'ordre & treillis

Soit $(P, \leqslant)$ un ordre partiel.
$\leqslant$ est **réflexive**, **symmétrique**, **transitive**.

On pose :
* $\sup X =$ la plus petite borne supérieure de $X$
* $\inf X =$ la plus grande borne inférieure de $X$

Un **treillis** est un ordre partiel $(P, \leqslant)$ tel que :
$$
\forall x, y \in P \quad \exists \sup(x, y) \quad \exists \inf(x, y)
$$

Un **treillis complet** est un ordre partiel $(P, \leqslant)$ tel que :
$$
\forall X \subseteq P \quad \exists \sup X
$$

Il existe des trellis non complets : par exemple, les chaines binaires et l'ordre lexicographique, ou les nombres naturels et l'ordre usuel. Tous les **treillis finis** sont **complets**. L'inverse n'est pas nécessairement vrai (tout espace projectif avec un point a l'infini). On appelle $\top$ (resp. $\bot$) le plus grand (resp. petit) élément d'un treillis complet.

On dira que $f : P \to P$ est **monotone** si :
$$
\forall x, y \in P \quad (x \leqslant y \implies f(x) \leqslant f(y))
$$

On dira que $x$ est un **point fixe de $f$** si $f(x) = x$.

On a le théorème suivant (Tarsky) : soit $f : P \to P$ monotone sur un treillis complet $P$.  Alors :
* $\operatorname{gfp}(f) = \sup \{ x \mid x \leqslant f(x) \}$

* $\operatorname{lfp}(f) = \inf \{ x \mid f(x) \leqslant x \}$

Si $P$ est un treillis fini, et $f$ est monotone, alors $\exists n, m$ tels que :
* $\operatorname{gfp}(f) = f^n(\top)$
* $\operatorname{gfp}(g) = f^m(\bot)$
