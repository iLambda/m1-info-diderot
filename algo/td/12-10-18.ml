let rec majoritaire t =
  let rec split l n = match l with
    | []           -> [], []
    | _ when n = 0 -> [], l
    | x::t         -> let l1, l2 = split t (n-1) in (x::l1, l2)
  in match t with
    | [] -> (None, 0)
    | [n] -> (Some n, 1)
    | _ -> begin
        let t1, t2 = split t ((List.length t)/2) in
        let t1x, t1n = majoritaire t1 in
        let t2x, t2n = majoritaire t2 in
          if t1x = t2x then (t1x, t1n + t2n)
          else if t1n > t2n && t1n > ((List.length t1) / 2) then (t1x, t1n)
          else if t2n > t1n && t2n > ((List.length t2) / 2) then (t2x, t2n)
          else (None, 0)
      end
