# Exercice 1

## Question 1

On va démontrer l'hypothèse :
> $\forall$ T tableau de longueur $< n$, `stoogeSort` trie correctement T

#### Initialisation ($n = 0$)

Trivialement, `stoogeSort(T)` trie les tableaux de taille 0 et 1 (ils sont tjrs triés). Si la longueur du tableau est 2, le cas est traité à part dans le cas `if fin - deb == 2`.

#### Itération ($\forall k \leq n, \mathscr P (k) \implies \mathscr P (n+1)$)

Par hypothèse de récurrence :
* après le 1er appel, `T[deb:b2]` est trié
* après le 2e appel, `T[b1:fin]` est trié
* après le 3e appel, `T[deb:b2]` est trié

Ainsi :
* après le 1er appel, $\color{limegreen}\bullet < \color{cornflowerblue}\bullet$
* après le 2er appel, $\color{cornflowerblue}\bullet < \color{plum}\bullet$, et
  on a $l(\color{limegreen}\bullet)$ et $l(\color{plum}\bullet) \leq l(\color{plum}\bullet)$

## Question 2

On a :
$$
T(n) = 3 \cdot T\left(\frac{2n}{3}\right) + 1
$$

Ainsi,
* $a = 3$
* $b = 3/2$
* $f(n) = 1$ (constante)

On remarque que :
$$
a \cdot f(n/b) = 3 \cdot f(n/b) = 3 \cdot f(n)
$$
Ainsi, on a $c = 3$. On est donc dans le cas :
$$
T(n) = \Theta(n^{\log_b a}) = \Theta(n^{\log_{1.5} 3}) = \Theta(n^{\log 3 / \log 1.5}) \approx \Theta(n^{2.709})
$$
Ce qui est pire que le bubble sort en $O(n^2)$.

## Question 3

# Exercice 2

## Question 1

Soit $T_1$ et $T_2$ des listes telles que $T = T_1 + T_2$.

>Dans la suite, on notera $\operatorname{count}_{L}(x)$ le nombre de $x$ dans la liste $L$.

On cherche à trouver comment la relation "$x$ majoritaire dans $L$" ($\iff \operatorname{count}_L(x) < \vert L \vert / 2$) se comporte par rapport a la concaténation de listes.

* Supposons $x$ majoritaire dans $T_1$ et dans $T_2$

  On a donc :

  $$
  \left\lbrace
  \begin{array}{ccc}
  \operatorname{count}_1(x) > \frac{\vert T_1 \vert}{2} \\
  \operatorname{count}_2(x) > \frac{\vert T_2 \vert}{2}
  \end{array}
  \right.
  $$

  Or, dans $T = T_1 + T_2$, on a :

  $$
  \begin{align}
  \operatorname{count}_{T}(x) &= \operatorname{count}_1(x) + \operatorname{count}_2(x)\\
  &> \frac{\vert T_1 \vert}{2} + \frac{\vert T_2 \vert}{2} \\
  &> \frac{\vert T_1 \vert + \vert T_2 \vert}{2} \\
  &> \frac{\vert T_1 + T_2 \vert}{2}\\
  &> \frac{\vert T \vert}{2}
  \end{align}
  $$

  Ainsi, on a prouvé que :
  $$
    x \mbox{ majoritaire dans } T_1 \mbox { et } T_2 \implies x \mbox{ majoritaire dans } T_1 \text{ ou dans } T_2
  $$

* Supposons $x$ majoritaire dans $T$

En prenant la contraposée de la proposition précédente, on a :
$$
x \mbox{ pas majoritaire dans } T_1 + T_2 \implies x \mbox{ pas majoritaire dans } T_1 \mbox { ou pas majoritaire dans } T_2
$$
