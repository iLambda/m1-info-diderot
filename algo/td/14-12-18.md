# Exercice 2

## Question 1

On remarque que si la propriété métrique est vraie, on a un graphe complet. On va donc assumer dans la suite que $G = K_n$.

On prend un arbre couvrant du graphe (complexité $\vert E \vert \log \vert E \vert$).
On va suivre l'arbre ainsi trouvé afin de visiter chaque sommet au moins une fois. Puis, on va éliminer de ce parcours les sommets déja visités.

## Question 2

Le poids de ce cycle hamiltonien est au plus 2 fois le poids de l'arbre couvrant minimal, par l'inégalité triangulaire. Or, on sait qu'un arbre couvrant a un poids inférieur ou égal a celui de tout cycle hamiltonien.

On a donc une 2-approximation.

# Exercice 1

On remarque les deux choses suivantes :

* Pour chaque arête $a$ du chemin optimal $\operatorname{opt}$, il existe une arête $\varphi(a)$ de $\gamma$ donné par l'algorithme glouton telle que :
  $$
  \left\{
    \begin{align}
    &w(\varphi(a)) \leqslant w(a)\\
    &a \text{ et } \varphi(a) \text{ ont une extrémité commune}
    \end{align}
  \right.
  $$

  En effet, si $a \in \gamma$, alors on peut prendre $\varphi(a) = a$. Sinon, on a une "corde" dans le cycle hamiltonien optimal. Cela signifie que l'algorithme glouton a déterminé qu'une des arêtes adjacentes a une des extrémités de $a$ a un coût plus petit : c'est le $\varphi(a)$ recherché.

* Pour toute arête $e$ de $\gamma$, il existe au plus 2 arêtes a de $\operatorname{opt}$ telles que :
  $$
  \varphi(a) = e
  $$

  En effet, si $e = (u, x)$ et $\varphi(a) = e$, alors $u$ est extrémité de $a$.

  Alors, si on note $\mathcal V = \{ \varphi(a) \mid a \in \operatorname{opt} \}$, on a :
  $$
  w(\mathcal V) \leqslant \sum_{a \in \operatorname{opt}} w(\varphi(a)) \leqslant \sum_{a \in \operatorname{opt}} w(a) = w(\operatorname{opt})
  $$

  $\mathcal V$ contient au moins la moitié des arêtes de $\gamma$.

On va ensuite prendre $G_1$ induit par $S_{n+1} = S_n \setminus \{ \text{ les origines des arêtes de } \mathcal V_0 \}$. On peut définir $\varphi_1 : \operatorname{opt}_1 \to \gamma$ avec $\varphi_1(a) \notin \mathcal V$.

On peut alors montrer que $w(\operatorname{opt}_1) \leqslant w(\operatorname{opt}_0)$ grâce a l'inégalité triangulaire. Récursivement, puisqu'on divise la taille de l'ensemble par 2, on va avoir une $O(\log n)$ approximation.
