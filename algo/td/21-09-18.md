# Exercice 1

Approche #1 : On a, pour chaque configuration de dame, $n$ possibilités; l'arbre a une arité de n.
$$
\implies n^n \mbox{ configurations}
$$

Approche #2 : On a un arbre binaire, chaque choix est un oui ou un non. On a $n^2$ cases (profondeur de l'arbre).
$$
\implies 2^{(n^2)} = (2^n)^n \mbox{ configurations}
$$

À premiere vue, la pire est la 2ème configuration. Mais, il est plus facile de couper des branches dans l'arbre #2; cela peut donc plus être efficace. Il faut effectuer le test pour savoir finalement la meilleure solution.

# Exercice 2

## Question 2.1

```ocaml
let lssc_len l =
  let rec aux l' b = match l', b with
    | [], _ -> 0
    | h::t, None                -> max (aux t b) (1 + aux t (Some h))
    | h::t, Some(x) when x <= h -> max (aux t b) (1 + aux t (Some h))
    | h::t, Some(x)             -> aux t b
  in aux l None
```

## Question 2.2

```ocaml
let lssc l =
  let maxlist l1 l2 =
    if (List.length l1) > (List.length l2) then l1 else l2
  in
  let rec aux l' b = match l', b with
    | [], _ -> []
    | h::t, Some(x) when x > h -> aux t b
    | h::t, _ -> maxlist (aux t b) (h::(aux t (Some h)))
  in aux l None
```

## Question 2.3

# Exercice 3

Etant donné une clique $G$, on a :
$$
G \cup p \mbox{ clique } \iff \forall g \in G, p \mbox{ relié à } g
$$

Une relation structurelle est alors :
$$
\operatorname{CLQMAX}(G) =
\max(\operatorname{CLQMAX}(\mathcal{V}(s_1)), \operatorname{CLQMAX}(G \setminus \{s_1\}))
$$
avec $\mathcal{V}(s)$ le graphe de voisinage de $s$, et $s_1$ un sommet pris aléatoirement dans le graphe $G$ passé en paramètre. Cela revient a construire la clique maximum soit :
* en regardant la clique maximale de G privé d'un sommet
* en regardant la clique maximale du voisinage
