<# Le master theorème : énoncé standard

Si on a la récurrence suivante :
$$
T(N) = a \cdot T\left(\frac{N}{b}\right) + f(N)
$$

Alors :
* $f(N) = \Theta(N^{\log_b a}) \implies T(N) = \Theta(N^{\log_b a} \cdot \log N)$
* $\exists \varepsilon > 0, f(N) = \Omega(N^{\log_b a + \varepsilon}) \implies T(N) = \Theta(f(n))$
* $\exists \varepsilon > 0, f(N) = O(N^{\log_b a - \varepsilon}) \implies T(N) = \Theta(N^{\log_b a})$

# Problèmes difficiles

On va s'intéresser à des problèmes dont on sait (à priori, modulo P=NP) qu'il est impossible de résoudre en temps polynomial. On peut néanmoins améliorer certains de ces problèmes en améliorant la base de la complexité (réduire $\alpha$ dans $O(\alpha^n)$ un maximum).

## Problème du stable maximum

Le problème du **stable** maximum consiste a trouver un sous ensemble de sommets $U$ de taille maximum d'un graphge $G$ tel que aucune arête n'existe entre les paires de sommets $u, v \in U$.

* en *entrée* : un graphe $G$
* en *sortie* : un stable $U$ de $G$

### Algorithme naïf

On va énumérer tous les sous-ensembles, et tester a chaque fois si ils forment un ensemble indépendant, et garder le plus grand. Chaque sous ensemble peut être vu comme un mot binaire (car $\vert \mathcal P (G) \vert = 2^{\vert G \vert})$. Pour tester si un sous ensemble donné est maximal, on effectue $O(n^2)$ opérations.

La complexité $T(N)$ est alors :
$$
T(n) = O(2^n \cdot n^2)
$$

### Algorithme récursif

Cet algorithme est récursif. La récurrence structurelle utilisée ici est la suivante : prenons un sommet $v$. La taille du stable maximum est alors le maximum entre :
* la taille du stable maximum de $G \setminus \mathscr V(v)$
* la taille du stable maximum de $G \setminus \{v\}$

```
EIM2(G):
  Si G est vide, retourne 0
  Sinon, choisir v sommet de G
    avec <- 1 + EIM2(G \ voisinage(v))
    sans <- EIM2(G \ {v})
    retourne max(avec, sans)
```

Etudions sa complexité. On a comme récurrence :
$$
\begin{align}
T(N) &\leq T(N-1) + T(N-1) + O(N^2)\\
&\leq 2T(N-1) + O(N^2)
\end{align}
$$

Ce qui nous donne :
$$
T(n) = O(2^n \cdot n^2)
$$

#### Optimisation 1

On remarque que si $\mathcal V (v) = \{v\}$, on effectue deux appels récursifs similaires. De plus, on sait dans ce cas que $v$ doit être dans le stable maximum, car il est isolé, et sa présence augmente la taille du stable en n'ajoutant aucune contrainte.

L'algorithme optimisé est ainsi :

```
EIM3(G):
  Si G est vide, retourne 0
  Sinon, choisir v sommet de G
    Si voisinage(v) = v, retourne 1 + EIM3(G \ {v})
    Sinon
      avec <- 1 + EIM3(G \ voisinage(v))
      sans <- EIM3(G \ {v})
      retourne max(avec, sans)
```

Etudions a nouveau la complexité. On a comme récurrence :
$$
T(N) = \left\{
        \begin{align}
          &T(N-1) + N^2 \mbox{ si on trouve } v \mbox{ isolé} \\
          &T(N-1) + T(N-2) + N^2 \mbox{ sinon}
        \end{align}
      \right.
$$

On remarque que la relation de récurrence est similaire à celle de fibonacci. On a alors :
$$
\begin{align}
&T(n) \leq \operatorname{Fibonacci}(n) \cdot n^2 = \varphi^n \cdot N^n\\
\end{align}
$$

D'ou :
$$
T(n) = O(\varphi^n \cdot N^2)
$$
On a amélioré notre complexité, car $\varphi \approx 1.62 < 2$

#### Optimisation 2

Si le voisinage de $v$ est réduit a un sommet $w$, alors, on a toujours interet a mettre $v$ dans le stable maximum. En effet, on a 2 cas :
* $w \notin$ l'ensemble indépendant retourné. On pourrait ajouter $v$ sans casser l'indépendance ; donc l'appel `avec` fait mieux
* $w \in$ l'ensemble indépendant retourné. Dans ce cas, on peut tjrs trouver un ensemble indépendant de même taille en replaçant $w$ par $v$.

```
EIM4(G):
  Si G est vide, retourne 0
  Sinon, choisir v sommet de G
    Si |voisinage(v)| <= 2, retourne 1 + EIM4(G\voisinage(v))
    Sinon
      avec <- 1 + EIM4(G \ voisinage(v))
      sans <- EIM4(G \ {v})
      retourne max(avec, sans)
```

Etudions a nouveau la complexité de cet algorithme. On a
$$
T(N) = \left\{
        \begin{align}
          &T(N-1) \mbox{ si } \vert \mathscr V(v) \vert = 1\\
          &T(N-2) \mbox{ si } \vert \mathscr V(v) \vert = 2\\
          &T(N-1) + T(N-\vert \mathscr V(v) \vert) + N^2 \mbox{ sinon}
        \end{align}
      \right.
$$

dans tous les cas, on a :

$$
T(N) \leq T(N-1) + T(N-3) + N^2
$$

Résolvons la récurrence suivante en supposant que $T(N) = \alpha^N$.
On a (ignorons le $N^2$ en le factorisant):
$$
\begin{align}
a^N &= a^{N-1} + a^{N-3}\\
\implies a^3 &= a^2 + 1\\
\implies a^3 &- a^2 - 1 = 0
\end{align}
$$

Une racine positive de ce polynôme est $\alpha \approx 1.46$. D'ou :
$$
T(n) = O(\alpha^n \cdot N^2) = O(1.46^N \cdot N^2)
$$

On a encore réduit la complexité.

#### Optimisation 3

L'idée est la suivante ; si tous les sommets ont un degré inférieur ou égal à 2, il est facile de trouver un stable maximum. Il reste a trouver un algorithme polynomial simple pour ce cas la (simple étant donné que pour ce type de graphes, on a doit des sommets isolés, soit des chaînes, soit des cycles). On a alors :
$$
T(N) \leq T(N-1) + T(N-4) + N^2
$$

Par le même calcul que précédemment (cf Optimisation 2), on obtient une complexité de :

$$
T(n) = O(\beta^n \cdot n^2) = O(1.38^n \cdot n^2)
$$
