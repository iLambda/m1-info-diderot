# Algorithmes 'difficiles'

On se pose, après avoir étudié le probleme de l'ensemble indépendant maximal, la question de savoir si il existe un "bon" algorithme pour traiter ce problème. Par bon, on entend que sa complexité est *polynomiale*.

Il existe d'autres problèmes "réputés difficiles" :
* $\operatorname{3COL}$ : le problème de la 3-colorabilité d'un graphe
* $\operatorname{CLIQUE}$ : le problème de la clique maximale (trouver la plus grande clique d'un graphe donné)
* $\operatorname{HAM}$ : le problème du circuit hamiltonien
* $\operatorname{SUBSETSUM}$ : le problème de la sous somme maximale d'un ensemble
* $\operatorname{SAT}$ : le problème de déterminer la satisfiabilité d'une formule booléenne
* etc...

On va chercher a exprimer une notion formelle permettant de comparer la difficulté des problèmes entre eux : c'est la notion de **réduction**.

## Problèmes de décision

Pour se simplifier, on va se concentrer sur les problèmes booléens, ou **problèmes de décision**. Ainsi, le problème aura cette forme suivante :

* *en entrée* : une instance $I$ (un graphe, un ensemble d'entiers, ...)
* *en sortie* : $\operatorname{ACCEPTE}$ ($\top$), ou $\operatorname{REJETTE}$ ($\bot$)

>Par exemple, pour les problèmes cités précedemment, on a :
>* $\operatorname{3COL}$
>   * en entrée : un graphe $G$
>   * en sortie : $\operatorname{ACCEPTE} \iff$ il existe une 3-coloration de $G$

>* $\operatorname{HAM}$
>   * en entrée : un graphe $G$
>   * en sortie : $\operatorname{ACCEPTE} \iff$  il existe un circuit hamiltonien dans $G$

### Formalisme

On va préférer écrire les problèmes de décision sous forme d'un **langage**. On va décrire un problème de décision $\operatorname{DEC}$ de propriété $\mathcal P$ comme :
$$
  DEC = \{ I \mid \mathcal P(I) \equiv \top  \}
$$

Le problème de décision étant donné une instance $I$ est alors :
$$
I \in \operatorname{DEC}
$$

### Classe $\mathscr P$

Un langage $L$ est **décidable en temps polynomial** s'il existe un algorithme qui le résoud correctement donc la complexité est polynomiale ; c'est à dire $O(N^c)$ avec $c$ une constante et $N$ la taille de l'instance $I$ (par exemple, la longueur d'une chaîne binaire qui encode/représente l'entrée). On écrira alors :
$$
L \in \mathscr P
$$

>Par exemple, le problème suivant :
>$$
\operatorname{PCC} = \{ G \mbox{ graphe}, (s,t) \in G,  d \in \mathbb N \mid G \mbox{ admet un chemin de longueur } \leq d \mbox{ de } s \mbox { à } t \}
>$$
> est polynomial (algorithme de Dijkstra) : on a donc $\operatorname{PCC} \in \mathscr P$.


## Structure commune des problèmes 'difficiles'

Les problèmes 'difficiles' énoncés ci-dessus se reformulent donc de la façon suivante :
* $\operatorname{3COL} = \{G \mid G \mbox { admet une 3-coloration}\}$
* $\operatorname{HAM} = \{G \mid G \mbox { admet un cycle hamiltonien }\}$
* $\operatorname{SUBSETSUM} = \{(x_1, ..., x_m, T) \mid \exists \mbox { un sous ensemble des } x_i \mbox{ de somme } T\}$
* $\operatorname{CLIQUE} = \{(G, t) \mid G \mbox { admet une clique de taille } t\}$
* $\operatorname{EI} = \{(G, t) \mid G \mbox { admet un ensemble indépendant de taille } \leq t\}$

Ces problèmes ont effectivement une structure commune :
$$
\{ X \mid \exists \pi \mbox{ dans } X \mbox { tel que } \pi \mbox{ a une structure particulière}\}
$$
Trouver une solution est complexe, car la méthode naïve est d'aller énumérer tous les $\pi$ potentiels, et l'ensemble des $\pi$ possibles a une cardinalité **exponentielle**. Mais, si on nous donne un $\pi$ potentiel, il est aisé (en temps **polynomial**) de vérifier qu'elle satisfait ou non la condition. On va donner un nom a l'ensemble des problèmes de cette forme.

### Formalisme

On appelera $V$ un **prédicat de vérification pour $L$** si :
$$
X \in L \iff \exists \pi \mbox { tq } V(X, \pi) \equiv \top
$$

### Classe $\mathscr{NP}$

Un langage $L$ appartient à la classe de problèmes $\mathscr{NP}$ si $L$ admet un **prédicat de vérification** $V$ polynomial, c'est à dire qu'il existe un algorithme permettant de calculer $V(N, \pi)$ ayant une complexité $O(N^c)$ (avec $N$ la taille de l'instance $X$, $c$ une constante). On écrira alors :
$$
L \in \mathscr{NP}
$$

>Par exemple, pour le problème $\operatorname{3COL}$, on va prendre :
>$$
\left\lbrace
\begin{array}{ccc}
\begin{align}
&X = \mbox{graphe}\\
&\pi = \mbox{coloration}\\
&V(X, \pi)=
\left\lbrace
\begin{array}{ccc}
\top \mbox{ si } \pi \mbox{ est une coloration légale}\\
\bot \mbox { sinon }
\end{array}
\right.
\end{align}
\end{array}\right.
>$$
> étant donné que $V(X, \pi)$ est évaluable en temps polynomial, on a alors :
>$$
\operatorname{3COL} \in \mathscr{NP}
>$$


## Comparaison de la difficulté des problèmes

Comment peut-on comparer la "difficulté" ou la "complexité" de ces problèmes ? On va définir un ordre partiel entre plusieurs langages.

Pour deux langages $L_1$, $L_2$ :
$$
L_1 \leq_P L_2 \iff \mbox{ résoudre } L_1 \mbox { revient a résoudre une instance de } L_2
$$
On dira que "$L_1$ n'est pas plus difficile que $L_2$". Ainsi, on dira que $L_1 \leq_P L_2$ si il existe une façon efficace $f$ de transformer une instance $X$ du probème $L_1$ en une instance $Y=f(X)$ du problème $L_2$, de sorte que :
$$
X \in L_1 \iff Y = f(X) \in L_2
$$

### Définition

L'**ordre partiel** $\leq_P$ est défini comme :

$$
L_1 \leq_P L_2 \iff
\left\lbrace
\begin{array}{ccc}
\begin{align}
&\exists f \mbox { de complexité polynomiale}\\
&x \in L_1 \leftrightarrow f(X) \in L_2
\end{align}
\end{array}\right.
$$

La fonction $f$ s'appelle une **réduction**, et on dit que **$L_1$ se réduit à $L_2$**.

On va donc pouvoir comparer la difficulté relative de nos problèmes.

### Relations entre $\mathscr P$ et $\mathscr{NP}$

*Attention* : ce n'est pas parce qu'on est dans $\mathscr{NP}$ qu'on est difficile.

> Par exemple, considérons le problème:
>$$
\operatorname{CYCLE} = \{ G \mid G \mbox{ contient un cycle } C \}
>$$
> il est aisé de vérifier qu'on a un cycle $C$ si il est donné. On a alors :
>$$
\operatorname{CYCLE}\in \mathscr{NP}
>$$
>Mais le problème est aussi polynomial, car il est aisé de décider $\operatorname{CYCLE}$ si $C$ n'est pas donné. Ainsi, on a :
>$$
\operatorname{CYCLE} \in \mathscr P
>$$

On a en réalité :
$$
\mathscr P \subseteq \mathscr{NP}
$$

Par contre, on a *à priori* pas :
$$
\mathscr P \cap \mathscr{NP} = \varnothing
$$


La situation est la suivante :

![](img/11-10-18_1.png)
