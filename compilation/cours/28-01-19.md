# Le runtime

Les programmes que nous allons compiler vont reposer sur un **runtime** qui contiendra des fonctions utiles qui gèreront par exemple l'allocation mémoire.

Ici, on aura un fichier C qui contiendra les prototypes suivants :
```c
location_t allocate_block(int64_t size);
value_t read_block(location_t block, int64_t index);
void write_block(location_t block, int64_t index, value_t v);
```

Il contiendra aussi des fonctions gérant les entrées/sorties, ou la comparaison de certains types de données.

## Fopix

Fopix est un langage un peu plus haut niveau que Retrolix. Il possède :
* des expressions
* flow control structuré
* indépendence vis-à-vis de l'architecture cible (pas de registres), gère directement les conventions d'appel de fonction

Pour l'assignation, on utilise des blocs avec la syntaxe :
```
block_e[index_e] := val_e (* assignation *)
block_e[index_e]          (* déréférence *)
```
Ils seront traduits par des appels a `write_block` et `read_block`.

### Difficultés de la traduction $\operatorname{Fopix} \to \operatorname{Retrolix}$

#### Passage des expressions au code à 3 addresses

Pour passer d'une expression à des instructions, la clé est d'avoir une variable de destination ou est stockée le résultat de l'expression.

On va avoir un programme qui va récursivement générer le code des sous expressions.

#### Structures de controle

#### Variables locales et masquage
