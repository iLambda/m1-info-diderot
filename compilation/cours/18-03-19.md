# Gestion automatique de la mémoire

L'état d'un programme Retrolix se compose des éléments suivants :
* registres HW
* variables (pile)
* blocs (tas)

Le lifetime des variables sur la pile est connu statiquement. Ce n'est pas le cas du tas. On va donc implémenter un **garbage collector**.

On a deux processus :
* le **mutateur**. Le code de l'utilisateur qui écrit/alloue/lit dans le tas. Son état se compose des éléments suivants:
  * registres HW
  * variables (pile)
  * blocs (tas)

* le **collecteur**. La partie du code qui ramasse effectivement la mémoire


Le tas est composé de **blocs**, qui sont eux même un ensemble de *champs* et d'un *en-tête* contenant de métadonnées. On peut voir le tas comme un **graphe** de blocs.

## Représentation du tas

Les **racines** sont des pointeurs vers le tas stockés en registre ou sur la pile.

Un bloc $B$ est **atteignable** s'il existe un chemin dans le tas partant d'une racine et finissant à $B$.

## Algorithmes de collection classiques

### Par marquage-balayage

Premièrement, on parcourt en profondeur tout le tas et on marque les blocs atteignables (marquage).

Puis, on parcourt tout le tas et on libère les blocs non marqués.

La complexité est proportionnelle au nombre total $n$ de blocs : $O(n)$.

### Par copie

On crée une copie ismorphe à la partie atteignable du tas.

La complexité est proportionnelle au nombre total $n$ de blocs atteignables : $O(n)$.

### Par comptage de références

On maintient le degré entrant de chaque bloc. Lorsque le compteur tombe à 0, on peut libérer le bloc.

Malheureusement, on ne peut pas libérer automatiquement des cycles. Le comptage de référence est aussi couteux.

## Techniques d'implémentation

### Représentation mémoire des blocs

On a besoin des primitives suivantes :
* calculer la taille d'un bloc
* parcourir les champs d'un bloc, distinguer les pointeurs
* parcourir les racines
* parcourir le tas : trouver le prochain bloc après un bloc donné

On va principalement stocker ces infos dans les métadonnées. La représentation de ces informations doit être efficace.

### Interface collecteur-compilateur

On appelle le collecteurs lorsqu'on alloue un bloc et qu'on a plus de mémoire disponible.

### Représentation du polymorphisme

On a plusieurs solutions.

#### Monomorphisme

On instancie la fonction polymorphe pour chaque instanciation de son quantificateur universel. Par exemple, si $\operatorname{id} : \forall \alpha, \  \alpha \to \alpha$ est utilisé avec $\operatorname{int}$ et $\operatorname{string}$, on crée deux fonctions.

Avantages :
* simplifier
* peu d'overhead

Désavantages :
* bcp de code
* pas tjrs possible (récursion polymorphe)
* compilation séparée ?

### Représentation uniforme

Tout est pointeur. Cette solution est régulière, mais couteuse en espace et en temps.

### Boxing des valeurs polymorphes

On va convertir une fonction $\operatorname{id} : \forall \alpha, \  \alpha \to \alpha$ en $\forall \alpha. box(\alpha) \to box(\alpha)$.

On ne pénalise pas le code monomorphe, et le code polymorphe est simple. Mais les coercions ont un cout, et peuvent vite s'accumuler lorsqu'on utilise par exemple des fonctions de première classe.

### Inspection des types a l'exécution

Les fonctions recoivent des arguments qui decrivent les types concrets. Cette solution est couteuse, et incompatible avec les conventions systèmes.

### Représentation uniforme sauf cas particulier

On ne boxe pas :
* les entiers (63 bits)
* les tableaux de flottants

Les valeurs polymorphes font 1 mot machine.

Avantages :
* plus pragmatique : on a la simplicité des représentations uniformes

Désavantages :
* complexifie légèrement l'arihmétique
* restreint a des types fixés
