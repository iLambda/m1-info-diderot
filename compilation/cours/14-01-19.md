#  Présentation de l'UE

Dans ce cours, on va développer un compilateur pour Hopix. C'est encore une fois un cours orienté projet, qui continue le projet d'interprétation des programmes du semestre 1.

## Projet

Le projet se décompose en deux étapes :
* Implémenter un compilateur naïf de Hopix vers x86-64
* Implémenter des optimisations de haut niveau vers bas niveau

La chaine de compilation :
$$
\texttt{Hopix} \to \texttt{Hobix} \to \texttt{Fopix} \to \texttt{Retrolix} \to \texttt{x86-64}
$$

### Compilateur $\texttt{Hopix} \to \texttt{Hobix}$

On va prendre comme exemple :

```
def len(l) =
  case l {
    | Nil => 0
    | Cons (_, xs) => 1 + len (xs)
  }
```

Cette première passe de compilation va essentiellement de supprimer les structures de données, et les remplacer par des blocs alloués dans le tas.

Pour transformer la liste en blocs, on va :
* étiqueter chaque constructeur par un entier $k$
* allouer un bloc qui contiendra $k$, puis les données utilisées dans le constructeur.

Elle devient en Hobix :
```
def len(l) =
  if l[0] = 0 then
    0
  else if l[0] = 1 then
    let xs = l[2] in
    1 + len(xs)
  else
```

### Compilateur $\texttt{Hobix} \to \texttt{Fopix}$

On va ici, expliciter les fermetures (closures) afin de pouvoir transformer un langage d'ordre supérieur en un langage du premier ordre.

Prenons :
```
def add (x) =
  let z = 2 * x in
  fun (y) -> x + (y * z)
```

On va expliciter la fermeture de la fonction :

```
def anon_0 (y, closure) =
  closure[2] + y * closure[1]

def add (x) =
  let z = 2 * x in
  [ ptr_code(anon_0) ; z ; x ]

```

On s'est donc ramenés à un langage de premier ordre.

### Compilateur $\texttt{Fopix} \to \texttt{Retrolix}$

Prenons :
```
def fact (n) =
  if n = 0 then 1 else n * fact (n-1)
```

Cette fonction deviendra :
```
def fact()
  locals tmp

  l0: cmp %rdi, 0 -> l1, l3
  l1: mov %rax, 1 -> l2
  l2: ret
  l3: tmp <- %rdi
  l4: %rdi <- %rdi - 1 -> l5
  l5: call fact -> 16
  l6: %rax <- mul tmp, %rax -> l7
  l7: ret
```

# Architecture x84/64

x86 est un jeu d'instructions (instruction set architecture, ISA). C'est une couche d'abstraction qui permet la portabilité sur une famille de processeurs ; en opposition avec la micro-architecture qui est l'implémentation d'une ISA (et les techniques d'implémentation).

Il y a deux grands styles d'ISA :
* RISC : reduced instruction set computer
* CISC : complex instruction set computer

Le style RISC, c'est d'avoir un petit jeu d'instruction simples et orthogonales, avec peu de redondance. On distinguera les instructions arithmétiques, logiques et mémoire.

x86-64 est répendu, rétrocompatible, et performant. Mais, il a les inconvénients d'être complexe et "baroque".

## Modélisation simplifiée

Un processeur x86-64 va avoir un état. Il sera constitué de :
* registres mémoire (program counter, ...)
* mémoire, dont la pile

### Registres

Dans ce qui va suivre, on va suivre la syntaxe de l'assembleur GNU. Il y a 16 registres généraux (entiers 64 bits little endian):
* %rax, %rbx, %rcx, %rdx, %rbp, %rsp, %rdi, %rsi, %r8, %r9, %r10, ..., %r15

Les registres %eax, %ebx, ... existent tjrs et pointent vers les 32 bits de poids faible du registre 64 bits correspondant. De même pour %ax (16 bits), %ah% et %al% (8 bits).

Le registre %rip est le program counter du processeur. Le registre %rflags est un champ de bits (flags) contenant des informations arithmétiques (overflow, ...)

La notation :
$$
\texttt{OFFSET}(\texttt{BASE}, \texttt{INDEX}, \texttt{SCALE}) = \texttt{OFFSET} + \texttt{BASE} + \texttt{INDEX} * \texttt{SCALE}
$$
permet de designer plus une adresse

#### Exemples

```
movq SRC, DST
```

L'instruction `mov[bwlq]` va déplacer le contenu de `SRC` dans `DEST`. b,w,l,q correspondent a byte, word, long, et quadword, respectivement 8, 16, 32 et 64 bits.

Par exemple :
```
movq $42, %rax # %rax contient 42
movq %rbx, -8(%rsp) %on envoie %rbx dans l'adresse %rsp-8
```

### Pile

On utilisera `%rsp` comme pointeur de pile.
La pile croît vers le bas. Le pointeur de pile doit être aligné sur 8 octets (càd `%rsp` doit être un multiple de 8).

On utilisera `%rbp` comme pointeur de cadre (frame pointer).

On peut utiliser les instructions `push` et `pop`, afin de pouvoir push et pop dans la pile. La syntaxe est `push SRC`, et `pop DST`.

L'instruction `pushq %rax` est équivalent à :
```
subq $8, %rsp
mov %rax (%rsp)
```
ou encore
```
mov %rax, -8(%rsp)
subq $8, %rsp
```

Les parenthèses () permettent de représenter la notation offset avec un offset de 0.

### Arithmétique, registre `%rflags`

On a `subq`, `addq`, `xorq` des instructions arithmétiques. Elles vont avoir un effet de bord supplémentaire : modifier la valeur de `%rflags`.

| Bit | Signification | Mnémonique
|--|--|--|
| 0 | Carry / retenue | CF
| 1 | Bit de parité | PF
| 6 | Zero | ZF
| 7 | Signe (1 ssi négatif) | SF 
| 11 | Overflow | OF

L'instruction `cmp SRC1 SRC2` va calculer SRC2 - SRC1, puis jeter le résultat, et mettre à jour `%rflags`. Certaines instructions vont ensuite lire `%rflags` afin d'effectuer des choses sous conditions (`je`, le saut conditionnel ssi ZF = 0).

### Convention d'appel

Pour appeler une fonction a l'étiquette `foo`, on va utiliser :
`call foo`. La différence avec `jmp` est qu'on va empiler l'adresse de retour st ockée dans `%rip` sur le stack. Un appel a `ret` nous renverra a l'adresse empilée.

On a une convention supplémentaire : à chaque `call`, on doit aligner `%rsp+8` soit aligné sur 16 octets.
