# TD7

## Exercice 7

### Question 1

* $\leqslant_m^p$ est **reflexive**; trivial en prenant comme réduction $f = \operatorname{id}$
* $\leqslant_m^p$ est **transitive**; si $A \leqslant_m^p B$ et $B \leqslant_m^p C$ et qu'on note $f$ et $g$ leurs réductions. La réflexion $g \circ f$ est toujours polynomiale, et permet de montrer que $A \leqslant_m^p C$

### Question 2

Supposons $B \in \operatorname{P}$ et $A \leqslant_m^p B$ par la réduction $f$.
Prenons la machine suivante :

```
Sur entrée a:
  Calculer b = f(a)
  Accepter ssi b ∈ B
```

L'étape 1 est polynomiale, car $f$ est polynomiale par hypothèse. L'étape 2 est polynomiale car $B \in \operatorname{P}$. La somme de deux fonctions polynomales est polynomiale, donc on a trouvé une MTD qui reconnait $A$ en temps poly, donc $A \in \operatorname{P}$.

### Question 3

#### $\operatorname{CLIQUE} \sim_m^p \operatorname{ENS-INDEP}$

Prenons la fonction suivante :
$$
f(G) = \overline G, \enspace \forall G \text{ graphe}
$$
Elle est calculable de complexité polynomiale ; en effet, il suffit d'inverser tous les coefficients de la machine d'adjacence.

On a l'identité suivante :
$$
X \text{ clique dans } G \iff \overline X \text{ ens-indep dans } \overline G
$$

D'ou :
$$
X \in \operatorname{CLIQUE} \iff f(X) \in \operatorname{ENS-INDEP}\\
f(X) \in \operatorname{CLIQUE} \iff X \in \operatorname{ENS-INDEP}
$$
(car $f$ est une involution, càd $f^2 = \operatorname{Id}$, donc $f$ inversible d'inverse $f$).

# TD 8

## Exercice 1

Supposons que $\operatorname{P} = \operatorname{NP}$

Prenons $A \in \operatorname{NEXP}$ ($\operatorname{NTIME}(2^{n^k})$).
Posons :
$$
\tilde A = \{ (x, 1^{2^{\vert x \vert^k} }) \mid x \in A \}
$$

On a $\tilde A \in \operatorname{NP}$ grâce au padding de l'entrée. Par hypothèse, $\tilde A \in \operatorname{P}$ par une MTD $M_{\tilde A}$ en temps $n^{k'}$.

En prenant l'algo suivant :
```
Sur entrée x:
  Calculer (x, 1^(2^|x|^k))
  Exécuter M_Atilde dessus
```
On montre que $A \in \operatorname{EXP}$.
