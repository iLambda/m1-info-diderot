# Exercice 1

## Question 1

N'importe quel parcours.

## Question 2

```
Sur entrée x:
  u <- s
  Pour i=0...n:
    Si v = t, ACC
    Deviner v voisin de u
    u <- v
  REJ
```

Cet algorithme finit toujours ; en effet, si il existe un chemin entre $s$ et $t$, alors il est de longueur $\leqslant n$.

On a besoin de trois cases mémoires afin de stocker $u$, $v$ et $i$. Elles stockent un entier binaire allant jusqu'a $n$; la complexité est $O(\log n)$.
$$
\implies \operatorname{CHEMIN} \in \operatorname{NL}
$$

## Question 3

D'après le th de Savitch, on a donc une complexité déterministe en espace de $O(\log^2 n)$.

# Exercice 2

* Montrons $\operatorname{NL} \subseteq \operatorname{P}$. On va d'abord montrer que $\operatorname{NSPACE}(s(n)) \subseteq \operatorname{DSPACE}(2^{O(s\left(n)\right)})$

  On va effectuer un parcours dans le graphe des configurations de $N(x)$ pour décider si il existe un chemin de $C_0$ à $C_{acc}$. La taille du graphe est égale au nombres max de configurations, qu'on a déja prouvé était $2^{O(s(n))}$. Le parcours se fait en temps polynomial, ce qui nous donne un algo en temps déterministe $2^{O(s(n))}$.

  Ainsi, on a $\operatorname{NL} = \operatorname{NSPACE}(\log n) \subseteq \operatorname{DTIME}(2^{O(\log n)}) \subseteq \operatorname{P}$

* On utilise le même argument pour prouver que $\operatorname{NPSPACE} \subseteq \operatorname{EXP}$

* $\operatorname{PSPACE} = \operatorname{NPSPACE}$ par le théorème de Savitch

* $\operatorname{NP} \subseteq \operatorname{PSPACE}$.

  On se place dans l'arbre des configurations d'une MTND $N$ de $\operatorname{NP}$. Puisqu'elle est dans $\operatorname{NP}$, on sait que sa profondeur est polynomiale. Donc, on va simuler l'éxécution de $N$ le long de tous les chemins possibles, ce qui va nous donner une complexité spatiale $\operatorname{PSPACE}$.

# Exercice 3

## Question 1

On calcule $g(n)$ sur R1. Ensuite, on calcule $f$ sur R2 en lisant R1. Sa complexité en espace est alors $s_g(\vert x \vert) + \vert g(x) \vert + s_f(\vert g(x) \vert)$.

## Question 2

L'idée est la suivante : on va recalculer à la volée chaque bit de $g(x)$ lorsque $f$ en a besoin afin d'éviter de stocker $g$.

On va d'abord trouver un algo en temps $s_g(n)$ pour $(x, i) \mapsto g(x)^i$, càd qui renvoie le $i$ ème bit du résultat en utilisant le même espace que l'algo original.
On va exécuter $M_g(x)$ en gardant un compteur pour la position de la tête sur le ruban de sortie. A chaque passage en position $i$, on met à jour le symbole qui aurait été écrit.

# Exercice 4

```
n = 0
Tant que on ne lit pas une case blanche:
  Si case = '(': n++
  Si case = ')': n--
  Si n < 0: REJ
ACC
```
