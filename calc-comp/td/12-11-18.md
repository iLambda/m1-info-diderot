On rappelle :
$$
\exists A \notin \operatorname{NP} \text{ tq. } A \text{ non } \operatorname{NP}\text{-dur}
$$
