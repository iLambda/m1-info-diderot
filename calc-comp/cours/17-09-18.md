# Automates non déterministes

## Introduction

On s'intéresse, lorsque $L$ et $L'$ sont réguliers, à l'opération $L \circ L'$ telle que :
$$
L \circ L' = \{ xy \mid x \in L, y \in L' \}
$$

On voudrait prouver que cette opération est fermée sous les langages réguliers (ce qui à fortiori sera le cas).

### Idée

Intuitivement, on a envie de prendre un automate $A$ reconnaissant $L$ et un automate $A'$ reconnaissant $L'$, et de les "coller ensemble" afin que une fois que le début du mot aie été reconnu par $A$, il passe par $A'$.

Seulement, il existe plusieurs façons ($n+1$ exactement) pour couper un mot $w$ de longueur n. Il est alors possible que pour chacune de ces coupures, le début du mot appartienne au langage $L$ mais que la fin n'appartienne pas à $L$, alors qu'une autre coupure ferait l'affaire. Si un découpage adéquat est donné, il est alors plus facile de vérifier que le mot appartient à $L \circ L'$. Il faudrait garder la trace des plusieurs passages dans l'automate afin de ne rien manquer.

Pour prouver la fermeture de $\circ$, on va alors introduire le concept d'*automates non déterministes*.

## Définition

Un **automate non déterministe** N se définit de la façon suivante :
$$
A = (Q, \Sigma, \delta, q_0, F)
$$
avec :
* $Q$ ensemble fini d'états
* $\Sigma$ alphabet
* $\delta$ fonction de transition, de signature $Q \times \Sigma \to \mathscr{P}(Q) = 2^Q$
* $q_0 \in Q$ état initial
* $F \subset Q$ états finaux

On va alors reconnaitre un mot $x$ ssi :
$$
x \in L(N) \iff \exists \mbox{ une séquence de transitions autorisée de } q_0 \mbox{ à } f \in F
$$

Ou, en définissant une fonction un peu modifiée $\delta^\ast$ de signature $2^Q \times \Sigma^\ast$ \to $2^Q$ définie par (en oubliant pas de l'itérer sur $a$ pour accepter des mots) :
$$
  \forall a \in \Sigma, R \subset Q, \delta^\ast (A, a) = \bigcup\limits_{q \in R} \delta(q, a)
$$
on aurait alors:
$$
x \in L(N) \iff \delta^\ast(\{q_0\}, x) \cap F \neq \varnothing
$$

# Relations d'équivalence

Une relation sur $E$ est un ensemble $R \subset E^2$. Par souci de notation, on dira que $x R y \iff (x, y) \in R$.
Une **relation d'équivalence** est une relation $R$ qui est :
* *réflexive*, i.e $\forall x \in E, x R x$
* *symétique*, i.e $\forall x,y \in E, x R y \iff y R x$
* *transitive*, i.e $\forall x, y, z \in E, x R y \land y R z \implies xRz$

La **classe d'équivalence** de $e \in E$ est l'ensemble $\cal{C}_e$ tel que :
$$
\forall x \in E, xRe \iff x \in \cal{C}_e
$$
Les classes d'équivalences de $R$ forment alors une **partition** de $E$, puisqu'elles sont toutes disjointes, et que $\forall x, y \in E, \cal{C}_x = \cal{C}_y \iff xRy$.
