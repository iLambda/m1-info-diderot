# Variantes de machines de turing

## Machine de turing à $k$ rubans

On se donne $k$ rubans, chacun munis de sa tête de lecture indépendante (qui partage leur état courant $q$). La fonction de transition aura la signature suivante :
$$
\delta : Q \times \Gamma^k \to Q \times \Gamma^k \times \{\rightarrow, \leftarrow\}^k
$$

Prouvons que :
$$
L \mbox{ accepté par une MT à } k \mbox{ rubans } \iff L \mbox{ accepté par une MT à 1 ruban }
$$

Le cas 1 ruban $\implies k$ ruban est trivial : il suffit de n'utiliser qu'un ruban sur les $k$.

#### Idée

Soit $M$ une MT à $k$ rubans. On peut représenter les $k$ rubans sur 1 ruban en entrelaçant les rubans : ainsi, chaque $k$-cellule sur le ruban de $M'$ représentera une case de chaque $k$ ruban.

Pour gérer ou se trouve la tête de lecture sur chacun des rubans, on va doubler l'alphabet. On va prendre $\Gamma' = \Gamma \cup \dot{\Gamma}$ avec $\dot \Gamma = \{\dot \sigma \mid \sigma \in \Gamma\}$.
On utilise $\dot \Gamma$ pour indiquer la position de la tête de lecture.

A chaque transition de $M$, il va falloir repérer le symbole pointé du $i$-ème sur le ruban $M'$ (à une position $p \equiv i \mod k$). On doit effectuer :
```
Pour chaque ruban Ri de M
  Repérer le symbole pointé de Ri
  S'en rappeler dans l'état
Effectuer la transition (changer symbole, déplacer point) sur chaque ruban
```

#### Complexité

Si $M$ s'exécute en temps $t$, alors $M'$ s'execute en temps $O(kt^2)$.

Par exemple, la reconnaissance des palindrômes se fait en $O(n)$ sur deux rubans. On peut montrer que $\Omega(n^2)$ opérations sont nécessaires pour les reconnaître sur 1 ruban.

## Machine de turing non déterministe

On va autoriser plusieurs transitions simultanées. La fonction de transition est alors :
La fonction de transition aura la signature suivante :
$$
\delta : Q \times \Gamma \to \mathscr P(Q \times \Gamma \times \{\rightarrow, \leftarrow\})
$$

Pour une entrée $x$ donnée à une MTND $N$, on peut représenter les exécutions possibles, à la façon d'un arbre.

$$
N \mbox{ accepte } x \iff \exists \mathscr C_0, \mathscr C_1, ..., \mathscr C_t \mbox{ tq } \left \{
  \begin{align}
    &\mathscr C_0 = q_0 x \\
    &\forall i, \mathscr C_i \vdash \mathscr C_{i+1}\\
    &\mathscr C_t \mbox{ est une configuration acceptante}
  \end{align}
\right. \\
$$

On peut avoir plusieurs exécutions rejetantes, ou même qui divergent. L'important est qu'il en existe **au moins une** acceptante.

On a :
$$
\forall N \mbox{ MTND}, \exists M \mbox { MT tq } L(M) = L(N)
$$
