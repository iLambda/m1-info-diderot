# Machines de Turning et calculabilité

On rappelle qu'une **machine de Turing** $M$ est un 7-uplet tel que :
$$
M = (Q, \Sigma, \Gamma, \delta, q_0, q_{acc}, q_{rej})
$$
* $Q$ un ensemble fini d'états
* $\Sigma$ un alphabet d'entrée ($\Sigma = \{0, 1\}$)
* $\Gamma$ un alphabet du *ruban*
* $\delta$ la fonction de transition.
$$
  \delta : Q \times \Gamma \to Q \times \Gamma \times \{G, D\}
$$
* $q_0 \in Q$ état initial
* $q_{acc} \in Q$ état spécial acceptant
* $q_{rej} \in Q$ état spécial rejetant

## Rappels

Un mot $w$ sera :
* **accepté** $\iff$ on arrive à $q_{acc}$ après un nombre fini de transitions
* **rejeté** $\iff$ on arrive à $q_{rej}$ après un nombre fini de transitions
* **diverge** $\iff$ a aucun moment on atteint $q_{acc}$ ou $q_{rej}$

## Le langage $\{0^n1^n \mid n \geq 0\}$

Intuitivement, sur notre ruban, on va noter le mot d'entrée.
L'idée est la suivante : on va aller à gauche du ruban chercher un 0, le supprimer, puis aller à droite, chercher le dernier 1, et le supprimer. Si on ne trouve plus de 0, le mot est dans $L$. Si on ne trouve plus de 1, le mot n'est pas dans $L$.

![](img/01-10-18_1.png)

On réalise un nombre $O(n^2)$ transitions afin de reconnaitre un mot de longueur $n$. La **complexité** sur une machine de turing sera représentée par le nombre de transitions réalisées sur un mot de longueur m. Ici, la complexité est donc $O(n^2)$.

## Définitions & vocabulaire

### Décidabilité & énumérabilité

On dira qu'un langage $A \subseteq \Sigma^\ast$ est **Turing-énumérable** si il existe une machine de turing $M$ telle que :
$$
A = L(M)
$$


On dira qu'un langage $A \subseteq \Sigma^\ast$ est **Turing-décidable** si il existe une machine de turing $M$ telle que :
$$
A = L(M) \mbox{ et } \forall x \in \Sigma^\ast, M \mbox{ accepte ou rejette } x \mbox{ (càd  } M \mbox{ ne diverge jamais) }
$$

### Etats de la machine de turing

Une **configuration** d'une machine de turing $M$ est une combinaison de son état, la position de la tête de lecture, et le contenu du ruban. On écrit $uqv$ la configuration suivante :
* $u \cdot v$ le contenu (non blanc) du ruban
* $q$ est l'état de la machine de turing, et la tête de lecture est positionée sur le premier symbole de $v$.

(( cf schéma ))

En utilisant cette notation:
* la **configuration initiale** est $\mathscr C_0 = \mathscr C_{init} = q_0 x$
* les **configurations acceptantes** sont
  $$
  uq_{acc}v \enspace \mbox{ avec } u, v \in \Gamma^\ast
  $$
* les **configurations rejetantes** sont
  $$
  uq_{rej}v \enspace \mbox{ avec } u, v \in \Gamma^\ast
  $$
* l'**évolution** s'écrira $\mathscr C \vdash \mathscr C'$, plus précisément :
  $$
  uaqbv \ \vdash \left\{
          \begin{align}
            uab'q'v &\mbox{ si } \delta(q, b) = (q', b', \rightarrow)\\
            uq'ab'v &\mbox{ si } \delta(q, b) = (q', b', \leftarrow)
          \end{align}
        \right.
  $$

  et

  $$
  qbv \ \vdash \left\{
    \begin{align}
      q'b'v &\mbox{ si } \delta(q, b) = (q', b', \leftarrow)\\
      b'q'v &\mbox{ si } \delta(q, b) = (q', b', \rightarrow)
    \end{align}
  \right.
  $$

### Acceptation

On va traduire l'acceptation d'un mot dans la notation présentée dans la partie précédente.

On dit que $M$ **accepte** $w$ si :
$$
\exists \mathscr C_0, \mathscr C_1, ..., \mathscr C_t \mbox{ avec } t \geq 0
$$
telles que :
$$
\left \{
  \begin{align}
    &\mathscr C_0 = q_0 w \\
    &\forall i, 0 \leq i \leq t, \mathscr C_i \vdash \mathscr C_{i+1}\\
    &\mathscr C_t \mbox{ est une configuration acceptante}
  \end{align}
\right. \\
$$
