Lemma add_0_l : forall n, 0 + n = n.
Proof.
  reflexivity.
Qed.

Lemma add_succ_l : forall n m, S n + m = S (n + m).
Proof.
  reflexivity.
Qed.

Lemma add_0_r : forall n, n + 0 = n.
Proof.
  induction n.
  - reflexivity.
  - simpl. f_equal. assumption.
Qed.

Lemma add_succ_r : forall n m, n + S m = S (n + m).
Proof.
  induction n.
  - reflexivity.
  - intros. simpl. f_equal. apply IHn.
Qed.

Lemma add_assoc : forall n m p, (n + m) + p = n + (m + p).
Proof.
  intros. 
  induction n.
  - reflexivity.
  - intros. simpl. rewrite IHn. reflexivity.
Qed.

Lemma add_comm : forall n m, n + m = m + n.
Proof.
  induction n.
  - intros. simpl. symmetry. apply add_0_r.
  - intros. simpl. symmetry. rewrite IHn. apply add_succ_r.
Qed.

Lemma mul_0_l : forall n, 0 * n = 0.
Proof.
  reflexivity.
Qed.

Lemma mul_succ_l : forall n m, S n * m = m + n * m.
Proof.
  reflexivity.
Qed.

Lemma mul_0_r : forall n, n * 0 = 0.
Proof.
  intros. 
  induction n.
  - reflexivity.
  - simpl. apply IHn.
Qed.

Lemma mul_succ_r : forall n m, n * S m = n + n * m.
Proof.