exception Empty

module type STREAM = sig
  type 'a t = ('a cell) Lazy.t
  and 'a cell = Cons of 'a * 'a t
  val empty : 'a t
  val cons : 'a -> 'a t -> 'a t
  val get : 'a t -> 'a * 'a t
end

module MyStream : STREAM = struct
  type 'a t = ('a cell) Lazy.t
  and 'a cell = Cons of 'a * 'a t
  let empty = lazy (raise Empty)
  let cons x s = lazy (Cons (x,s))
  let get s = match s with lazy (Cons (x,s')) -> (x,s')
end
