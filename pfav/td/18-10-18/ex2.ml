exception Empty

module MyStream = struct
  type 'a t = ('a cell) Lazy.t
  and 'a cell = Cons of 'a * 'a t
  let empty = lazy (raise Empty)
  let cons x s = lazy (Cons (x,s))
  let get s = match s with lazy (Cons (x,s')) -> (x,s')
  let of_list l =
    let to_stream elem acc =
      cons elem acc
    in List.fold_right to_stream l empty

  let rec take n stream = match n with
    | 0 -> []
    | _ when n > 0 -> begin match get stream with
        | exception Empty ->  []
        | el, st -> el::(take (n-1) st)
      end
    | _ -> failwith "Can't take a negative number of elements"

  let rec from n = lazy (Cons(n, (from (n+1))))

  let rec no_multiple n stream =
    let (head, tail) = get stream in
     match head with
      | m when m mod n == 0 ->  no_multiple n tail
      | _ -> lazy (Cons (head, (no_multiple n tail)))

  let sieve () =
    let rec sieve_rec stream =
      let (head, tail) = get stream in
      lazy (Cons (head, sieve_rec (no_multiple head tail)))
    in sieve_rec (from 2)


end
