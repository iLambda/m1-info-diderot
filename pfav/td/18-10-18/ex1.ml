exception Empty

module type STREAM = sig
  type 'a t = ('a cell) Lazy.t
  and 'a cell = Cons of 'a * 'a t
  val empty : 'a t
  val cons : 'a -> 'a t -> 'a t
  val get : 'a t -> 'a * 'a t
end

module MyStream : STREAM = struct
  type 'a t = ('a cell) Lazy.t
  and 'a cell = Cons of 'a * 'a t
  let empty = lazy (raise Empty)
  let cons x s = lazy (Cons (x,s))
  let get s = match s with lazy (Cons (x,s')) -> (x,s')
end

let of_list l =
  let to_stream elem acc =
    MyStream.cons elem acc
  in List.fold_right to_stream l MyStream.empty

let rec take n stream = match n with
  | 0 -> []
  | _ when n > 0 -> begin match MyStream.get stream with
      | exception Empty ->  []
      | el, st -> el::(take (n-1) st)
    end
  | _ -> failwith "Can't take a negative number of elements"

let rec from n = lazy (MyStream.Cons(n, (from (n+1))))
