module type RALItf =
  sig
    type 'a t                           (* the type of our random-access lists *)
    val empty : unit -> 'a t                    (* an empty random-access list *)
    val length : 'a t -> int            (* how many elements in this list ? *)
    val cons : 'a -> 'a t -> 'a t       (* add an extra element on the left *)
    val decons : 'a t -> 'a * 'a t      (* leftmost element and rest *)
    val nth : 'a t -> int -> 'a         (* access to the n-th element in the list *)
  end

module RALList : RALItf =
  struct
    type 'a t = 'a list

    let empty () = []

    let rec length l = match l with
      | []   -> 0
      | h::t -> 1 + (length t)

    let cons x l = x::l

    let decons l = match l with
      | [] -> raise Not_found
      | hd::tl -> hd, tl

    let nth l n =
      let rec aux l i = match l with
        | []                  -> raise Not_found
        | _ when i < 0        -> raise Not_found
        | hd::tl when i = 0   -> hd
        | hd::tl              -> aux tl (i-1)
      in aux l n
  end

module RALEfficient (T: RALItf) : RALItf =
  struct
    type 'a t = ('a T.t) * int

    let empty () = (T.empty ()), 0

    let rec length l = match l with
      | _, x -> x

    let cons x l = let li, len = l in ((T.cons x li), (len + 1))

    let decons l =
      let li, len = l in
      let obj, dli = T.decons li
      in obj, (dli, (len - 1))

    let nth l n = let li, len = l in T.nth li n
  end

module RALBTree : RALItf =
  struct
    type 'a btree =
      | Leaf of 'a
      | Node of 'a btree * 'a btree
    type 'a t = (('a btree) * int) list

    let empty () = []

    let cons x l =
      (* aux ajoute t a la liste l*)
      let rec aux t l = match l with
        | [] -> [ t ]                       (*la liste est vide*)
        | (t0, s0)::tl when let tU, sU = t in s0 = sU ->
            let tU, sU = t in aux (Node(tU, t0), s0 * 2) tl
        | (t0, s0)::tl -> t::(t0, s0)::tl
      in aux (Leaf(x), 1) l

    let nth l n =
      let rec find_in_tree tree s i = match tree with
        | Leaf(d)    -> assert(i=0); d
        | Node(l, r) when i < s/2 -> find_in_tree l (s/2) i
        | Node(l, r) -> find_in_tree l (s/2) (i - (s/2))
      in let rec find_tree bl i = match bl with
          []   -> raise Not_found
        | (tree, s)::t when i < s -> (find_in_tree tree s i)
        | (tree, s)::t -> find_tree t (i - s)
      in find_tree l n

    let rec length l = match l with
      | [] -> 0
      | (tree, size)::t -> size + (length t)

    let decons l = failwith "TODO"


  end
