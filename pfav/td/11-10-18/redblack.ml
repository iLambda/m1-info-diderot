type color = R | B
type 'a tree =
 | E
 | T of color * 'a tree * 'a * 'a tree

let rec is_bst t = match t with
  | E -> true
  | T (_, l, v, r) -> (is_bst l) && (is_bst r) && (match l, r with
      | E, E -> true
      | T(_, _, lval, _), E -> lval < v
      | E, T(_, _, rval, _) -> v < rval
      | T(_, _, rval, _), T(_, _, lval, _) -> lval < v && v < rval
     )
