module type DequeItf =
  sig
  type 'a queue

  val empty : 'a queue
  val is_empty : 'a queue -> bool

  (* insert or extract the front element *)
  val cons : 'a -> 'a queue -> 'a queue
  val decons : 'a queue -> 'a * 'a queue (* raises Not_found if empty *)

  (* insert or extract the rear element *)
  val snoc : 'a queue -> 'a -> 'a queue
  val snoced : 'a queue -> 'a queue * 'a (* raises Not_found if empty *)
end

module Deque : DequeItf =
  struct

  type 'a queue = ('a list) * ('a list)

  let rec split l n = match l with
    | []           -> [], []
    | _ when n = 0 -> [], l
    | x::t         -> let l1, l2 = split t (n-1) in (x::l1, l2)

  let empty = [], []

  let is_empty q = match q with
    | [], []    -> true
    | _         -> false

  let cons x q = match q with
    | left, right -> x::left, right

  let snoc q x = match q with
    | left, right -> left, x::right

  let decons q = match q with
    | x::left, _ -> x, q
    | [], []     -> raise Not_found
    | [], right  -> begin
        let newleft, newright = split right ((List.length right)/2) in
        match newleft with
          | [] -> failwith "Impossible"
          | x::toadd -> (x, ((List.rev toadd), newright))
      end

  let snoced q = match q with
    | _, x::right -> q, x
    | [], []     -> raise Not_found
    | left, []  -> begin
        let newleft, newright = split left ((List.length left)/2) in
        match newright with
          | [] -> failwith "Impossible"
          | x::toadd -> ((newleft, (List.rev toadd)), x)
      end

  end
