type variable_index = int
type bdt =
| True
| False
| If of variable_index * bdt * bdt
type bdd = bdt

type binop = And | Or
type formula =
| Cst of bool
| Var of variable_index
| Neg of formula
| Binop of binop * formula * formula

let rec bdt_not = function
  | True -> False
  | False -> True
  | If (v, g, d) -> If (v, bdt_not g, bdt_not d)

let rec bdt_op v g d = match v with
  | And -> begin match g, d with
      | False, _ | _, False -> False
      | True, v | v, True -> v
      | If(v1, gg, gd), If(v2, _, _) when v1 < v2 -> If (v1, (bdt_op And gg d), (bdt_op And gd d))
      | If(v1, _, _), If(v2, dg, dd) when v1 > v2 -> If (v1, (bdt_op And g dg), (bdt_op And g dd))
      | If(v1, gg, gd), If(v2, dg, dd) -> If (v1, (bdt_op And gg dg), (bdt_op And gd dd))
    end
  | Or -> begin match g, d with
      | True, _ | _, True -> True
      | False, v | v, False -> v
      | If(v1, gg, gd), If(v2, _, _) when v1 < v2 -> If (v1, (bdt_op Or gg d), (bdt_op Or gd d))
      | If(v1, _, _), If(v2, dg, dd) when v1 > v2 -> If (v1, (bdt_op Or g dg), (bdt_op Or g dd))
      | If(v1, gg, gd), If(v2, dg, dd) -> If (v1, (bdt_op Or gg dg), (bdt_op Or gd dd))
    end

let rec bdt_of_formula = function
  (* Trivial cases *)
  | Cst true -> True
  | Cst false -> False
  | Var v -> If (v, True, False)
  (* Operations *)
  | Neg f -> bdt_not (bdt_of_formula f)
  | Binop (b, g, d) -> bdt_op b (bdt_of_formula g) (bdt_of_formula d)

let rec bdt_norm = function
  | True -> True
  | False -> False
  | If(v, g, d) ->
      let g' = bdt_norm g in
      let d' = bdt_norm d in
      if g' = d' then g'
      else If(v, g', d')
