module type RandomAccessListItf =
  sig
    val empty : unit -> 'a list
    val cons : 'a -> 'a list -> 'a list
    val decons : 'a list -> 'a * 'a list              (* may raise Not_found *)
    val nth : 'a list -> int -> 'a                    (* may raise Not_found *)
    val update_nth : 'a list -> int -> 'a -> 'a list  (* may raise Not_found *)
  end

(*Ex 1*)
module RandomAccessListArray : RandomAccessListItf =
  struct
    let empty () = []

    let cons x l = x::l

    let decons l = match l with
      | [] -> raise Not_found
      | hd::tl -> hd, tl

    let nth l n =
      let rec aux l i = match l with
        | []                  -> raise Not_found
        | _ when i < 0        -> raise Not_found
        | hd::tl when i = 0   -> hd
        | hd::tl              -> aux tl (i-1)
      in aux l n

    let update_nth l n x =
      let rec aux l i x = match l with
        | []                 -> raise Not_found
        | _ when i < 0       -> raise Not_found
        | hd::tl when i = 0  -> x::tl
        | hd::tl             -> hd::(aux tl (i-1) x)
      in aux l n x
  end

  (**)
  module RandomAccessListBList : RandomAccessListItf =
    struct
      type 'a btree =
        | Leaf of 'a
        | Node of 'a btree * 'a btree
      type 'a blist = (('a btree) * int) list

      let empty () = []

      let cons x l =
        (* aux ajoute t a la liste l*)
        let rec aux t l = match l with
          | [] -> [ t ]                       (*la liste est vide*)
          | (t0, s0)::tl when let tU, sU = t in s0 = sU ->
              let tU, sU = t in aux (Node(tU, t0), s0 * 2) tl
          | (t0, s0)::tl -> t::(tree, size)::tl
        in aux (Leaf(x), 1) l

      (*let decons l = match l with
        | [] -> raise Not_found
        | hd::tl -> hd, tl*)


      let nth l n =
       let tree = () in
       let rec aux l i acc =
        | [] -> raise Not_found
        | (t, s)::t when i >= acc && i < acc + s -> tree 
      in aux l n 0

      (*let update_nth l n x =
        let rec aux l i x = match l with
          | []                 -> raise Not_found
          | _ when i < 0       -> raise Not_found
          | hd::tl when i = 0  -> x::tl
          | hd::tl             -> hd::(aux tl (i-1) x)
        in aux l n x
    end*)
